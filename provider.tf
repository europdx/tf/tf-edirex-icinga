# Configure the Icinga2 provider
provider "icinga2" {
  api_url                  = "https://monitor.ics.muni.cz:5665/v1"
  api_user                 = "${var.icinga2_api_user}"
  api_password             = "${var.icinga2_api_password}"
  insecure_skip_tls_verify = true
}

provider "openstack" {
  auth_url = "https://identity.cloud.muni.cz/v3"
  region   = "brno1"
}
