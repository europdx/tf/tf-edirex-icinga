resource "icinga2_hostgroup" "edirex" {
  name         = "edirex"
  display_name = "Edirex hosts"
}

resource "icinga2_host" "dataportal-dev" {
  hostname      = "dataportal-dev.edirex.ics.muni.cz"
  address       = "dataportal-dev.edirex.ics.muni.cz"
  groups        = ["${icinga2_hostgroup.edirex.name}"]
  check_command = "hostalive"
  templates     = ["mail-service-notification"]

  vars {
    os        = "linux"
    osver     = "1"
    allowance = "none"
  }
}

resource "icinga2_service" "ssh" {
  name          = "ssh"
  hostname      = "${icinga2_host.dataportal-dev.hostname}"
  check_command = "ssh"
}

resource "icinga2_service" "http" {
  name          = "http2"
  hostname      = "${icinga2_host.dataportal-dev.hostname}"
  check_command = "http"
}

resource "icinga2_user" "rpesa" {
  name  = "rpesa"
  email = "pesa@ics.muni.cz"
}

resource "icinga2_notification" "daportal-dev-notification" {
  hostname = "${icinga2_host.dataportal-dev.hostname}"
  command  = "mail-host-notification"
  users    = ["rpesa"]
}

resource "icinga2_notification" "daportal-dev-http-notification" {
  hostname    = "${icinga2_host.dataportal-dev.hostname}"
  command     = "mail-service-notification"
  users       = ["rpesa"]
  servicename = "${icinga2_service.http.name}"
}
