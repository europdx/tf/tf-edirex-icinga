terraform {
  backend "swift" {
    container         = "edirex_icinga_terraform_state"
    archive_container = "edirex_icinga_terraform-state-archive"
    auth_url          = "https://identity.cloud.muni.cz/v3"
  }
}
