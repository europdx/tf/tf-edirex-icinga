variable "las_hosts" {
  type = "map"

  default = {
    "las.edirex.ics.muni.cz"   = "las.edirex.ics.muni.cz"
    "nki.edirex.ics.muni.cz"   = "nki.edirex.ics.muni.cz"
    "cruk.edirex.ics.muni.cz"  = "cruk.edirex.ics.muni.cz"
    "curie.edirex.ics.muni.cz" = "curie.edirex.ics.muni.cz"
    "kul.edirex.ics.muni.cz"   = "kul.edirex.ics.muni.cz"
    "vhio.edirex.ics.muni.cz"  = "vhio.edirex.ics.muni.cz"
  }
}

resource "icinga2_host" "las_hosts" {
  count         = "${length(var.las_hosts)}"
  hostname      = "${element(keys(var.las_hosts), count.index)}"
  address       = "${lookup(var.las_hosts, "${element(keys(var.las_hosts), count.index)}")}"
  groups        = ["${icinga2_hostgroup.edirex.name}"]
  check_command = "hostalive"
  templates     = ["mail-service-notification"]
}

resource "icinga2_service" "las_ssh" {
  count         = "${length(var.las_hosts)}"
  hostname      = "${element(keys(var.las_hosts), count.index)}"
  name          = "ssh"
  check_command = "ssh"
}

resource "icinga2_service" "las_http" {
  count         = "${length(var.las_hosts)}"
  name          = "http"
  hostname      = "${element(keys(var.las_hosts), count.index)}"
  check_command = "http"
}

#resource "icinga2_service" "las_https" {
#  count         = "${length(var.las_hosts)}"
#  name          = "https"
#  check_command = "http --ssl"
#  vars = {
#    http_ssl = true
#  }
#}

resource "icinga2_service" "las_load" {
  count         = "${length(var.las_hosts)}"
  name          = "load"
  hostname      = "${element(keys(var.las_hosts), count.index)}"
  check_command = "load"
}

#resource "icinga2_service" "las_disk" {
#  count         = "${length(var.las_hosts)}"
#  name          = "disk"
#  check_command = "nrpe!check_disk"
#}

